still in development(Auth section)...

## Back-End: ##
 * Ruby(bundler,rake)
 * EM(web-sockets)
 * Redis

## Front-end: ##
 * AnjularJS +ngRoute,ngAnimation
 * bootstrap
 * RequireJS
 * coffeeScript
 * Sass

## Tools ##
 * bower
 * rake
 * bundler
